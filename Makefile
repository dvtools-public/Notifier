COMPILER = cc
SOURCE = dvnotifier.c
CFLAGS = -O3
LDFLAGS = -lXm -lX11 -lXt -lXpm -lXinerama
TARGET = ./dvnotifier
INSTDIR = /usr/local/bin

.PHONY: all clean install uninstall

all: $(TARGET) $(TARGET2)

$(TARGET): $(SOURCE) 
	$(COMPILER) $(CFLAGS) $^ $(LDFLAGS) -o $@

clean:
	rm -f $(TARGET) $(TARGET2)

install: $(TARGET)
	install -m 755 $(TARGET) $(INSTDIR)

uninstall:
	rm -f $(INSTDIR)/$(notdir $(TARGET))
