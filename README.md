### Notifier

Graphical notifier window for X11 with a Motif GUI. It's meant to be called from other programs or scripts where a very simple info dialog is needed.


![screenshot](screenshots/dvnotifier_sample.png)



![screenshot](screenshots/dvnotifier_sample2.png)

#### Features

- Supports an Info, Warning or Error icon.
- Centering flag places window in the middle of first monitor.
- Blocking flag prevents interaction with other windows until Notifier is dismissed.
- Geometry flag for custom window location.

Tip: If calling Notifier from another Motif program, you can grab the position and size of the top level shell and then calculate the center position of your window. These values can then be passed to Notifier using the geometry flag. This puts the popup window in the middle of your application window.

#### Requirements 

- Motif 2.3.8
- X11/Xlib
- libXpm
- libXinerama

#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
./dvnotifier -m "My message string goes here!"
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```

#### License

This software is distributed free of charge under the BSD Zero Clause license.