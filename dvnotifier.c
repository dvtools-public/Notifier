
#include <stdio.h>
#include <stdlib.h>

#include <Xm/Xm.h> /* motif core */
#include <Xm/MwmUtil.h> /* for mwm function/button manipulation */
#include <Xm/Form.h> /* mainForm as container widget */
#include <Xm/Frame.h> /* used for decorative message frames */
#include <Xm/Label.h> /* message labels */
#include <Xm/PushB.h> /* dismiss button */

#include <X11/xpm.h>
#include <X11/extensions/Xinerama.h> /* window centering */

#include "icon_data.h"

/* exit when dismiss is pressed */
void dismiss_callback() { exit(0); }


static String fallback_x11_resources[] = 
{
	/* some optional font settings */
	"*renderTable: xft",
	"*xft*fontType: FONT_IS_XFT",
	"*xft*fontName: Roboto",
	"*xft*fontSize: 11",
	"*xft*autohint: 1",
	"*xft*lcdfilter: lcddefault",
	"*xft*hintstyle: hintslight",
	"*xft*hinting: True",
	"*xft*antialias: 1",
	"*xft*rgba: rgb",
	"*xft*dpi: 96",
	
	"*msgLabel*fontStyle: Bold",
	NULL,
};

/* main function */
int main(int argc, char *argv[])
{

/* handle help flags and print usage examples */
	for (int i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "-help") == 0  || strcmp(argv[i], "--help") == 0)
		{
			printf("\033[7m%s\033[0m\n", "\n DeskView Notifier v1.2 ");
			printf("\n  * An Info icon is the default if no other type is specified.\n");
			printf("\n  * The window manager will be responsible for client placement");
			printf("\n    if geometry or centering flags are not specified.\n");
			printf("\n  * Space key activates the Dismiss button.\n\n");
			printf("\033[7m%s\033[0m\n", "\n Options: \n");
			printf("  -h -help       Print this exact help text.\n");
			printf("  -e -error      Use the Error icon.\n");
			printf("  -w -warn       Use the Warning icon.\n");
			printf("  -m -message    Message string to be displayed.\n");
			printf("  -b -blocking   Prevent interaction with other windows until dismissed.\n");
			printf("  -c -center     Center window on first monitor.\n");
			printf("  -g -geometry   Standard X11 geometry option.\n\n");
			printf("\033[7m%s\033[0m\n", "\n Examples: \n");
			printf("  ~$ dvnotifier -error -message \"Illegal operation!\" -blocking\n");
			printf("  ~$ dvnotifier -warn -message \"$(sensors -f)\" -geometry +145+100\n");
			printf("  ~$ dvnotifier -message \"$(lscpu | fold -s -w 110)\" -center\n\n");
			printf("  ~$ dvnotifier -e -m \"Illegal operation!\" -b\n");
			printf("  ~$ dvnotifier -w -m \"$(sensors -f)\" -g +145+100\n");
			printf("  ~$ dvnotifier -m \"$(lscpu | fold -s -w 110)\" -c\n\n\n");
			
			return 0;
		}
	}
	

/* define xt context */
	XtAppContext app;

/* open top level */
	Widget topLevel = XtVaAppInitialize(&app, "dvnotifier", NULL, 0, &argc, argv, fallback_x11_resources, NULL);
	XtVaSetValues(topLevel,
		XmNtitle, "Notifier",
		XmNminWidth, 220,
		XmNminHeight, 98,
	NULL);
	
	/* tell vendorshell (toplevel) to not use min/max buttons */
	int decor;
	XtVaGetValues(topLevel, XmNmwmDecorations, &decor, NULL);
		decor &= ~MWM_DECOR_MENU;
		decor &= ~MWM_DECOR_BORDER;
	XtVaSetValues(topLevel, XmNmwmDecorations, decor, NULL);
	
	/* this will make the min/max functions disappear while keeping move/close */
	int func;
	XtVaGetValues(topLevel, XmNmwmFunctions, &func, NULL);
		func &= ~MWM_FUNC_CLOSE;
		func &= ~MWM_FUNC_MOVE;
	XtVaSetValues(topLevel, XmNmwmFunctions, func, NULL);


/* create main form in toplevel */
	Widget mainForm = XmCreateForm(topLevel, "mainForm", NULL, 0);
	XtVaSetValues(mainForm,
		XmNshadowThickness, 1,
	NULL);


/* create icon pixmaps */
	Pixmap info_pixmap, info_mask;
	XpmCreatePixmapFromData(XtDisplay(topLevel), RootWindowOfScreen(XtScreen(topLevel)), 
	info_xpm, &info_pixmap, &info_mask, NULL);

	Pixmap error_pixmap, error_mask;
	XpmCreatePixmapFromData(XtDisplay(topLevel), RootWindowOfScreen(XtScreen(topLevel)), 
	error_xpm, &error_pixmap, &error_mask, NULL);
	
	Pixmap warn_pixmap, warn_mask;
	XpmCreatePixmapFromData(XtDisplay(topLevel), RootWindowOfScreen(XtScreen(topLevel)), 
	warn_xpm, &warn_pixmap, &warn_mask, NULL);
	
	
/* create icon frame */
	Widget iconFrame = XtVaCreateManagedWidget("iconFrame", xmFrameWidgetClass, mainForm,
		XmNshadowThickness, 2,
		XmNheight, 42,
		XmNwidth, 42,
		XmNx, 8,
		XmNy, 8,
		XmNshadowType, XmSHADOW_IN,
		/* use info as default icon */
		XmNbackgroundPixmap, info_pixmap,
	NULL);


/* create dismiss button up here so we can set attachment points easily */
	Widget dismissButton = XtVaCreateManagedWidget("Dismiss", xmPushButtonWidgetClass, mainForm,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		XmNrightOffset, 8,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
	NULL);
	
	/* specify callback for the dismiss button */
	XtAddCallback(dismissButton, XmNactivateCallback, dismiss_callback, NULL);

/* set icon type based on a given command line arg */
	for (int i = 1; i < argc; i++) 
	{
		if (strcmp(argv[i], "-e") == 0 || strcmp(argv[i], "-error") == 0)
		{
			XtVaSetValues(iconFrame, XmNbackgroundPixmap, error_pixmap, NULL);
			
			break;
		}
		
		else if (strcmp(argv[i], "-w") == 0 || strcmp(argv[i], "-warn") == 0)
		{
			XtVaSetValues(iconFrame, XmNbackgroundPixmap, warn_pixmap, NULL);
			
			break;
		}
	}


/* grab the input string for the message label */
	int stringIndex = -1;
	
	for (int i = 1; i < argc; i++) 
	{
		if (strcmp(argv[i], "-m") == 0 || strcmp(argv[i], "-message") == 0)
		{
			stringIndex = i;
			
			break; 
		}
	}

	/* message text goes into input_string */
	char* input_string = argv[stringIndex + 1];

/* create frame for the message text */
	Widget msgFrame = XtVaCreateManagedWidget("msgFrame", xmFrameWidgetClass, mainForm,
		XmNshadowThickness, 2,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, iconFrame,
		XmNrightAttachment, XmATTACH_FORM,
		XmNtopAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNshadowType, XmSHADOW_ETCHED_IN, /* default to normal frame style */
		XmNleftOffset, 6,
		XmNrightOffset, 8,
		XmNtopOffset, 8,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, dismissButton,
		XmNbottomOffset, 5,
	NULL);
	
/* create message label inside frame */
	Widget msgLabel = XmCreateLabel(msgFrame, "msgLabel", NULL, 0);
	/* create XmString for message label */
	XmString msgLabelString = XmStringCreateLocalized(input_string);
	XtVaSetValues(msgLabel,
		XmNlabelString, msgLabelString,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNtopAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNmarginLeft, 6,
		XmNmarginRight, 6,
	NULL);
	/* free string */
	XmStringFree(msgLabelString);
	XtManageChild(msgLabel);


	/* close main form */
	XtManageChild(mainForm);

	/* close top level */
	XtRealizeWidget(topLevel);


/* make sure only this window gets focus until closed if -blocking arg is passed */
	for (int i = 1; i < argc; i++) 
	{
		if (strcmp(argv[i], "-b") == 0 || strcmp(argv[i], "-blocking") == 0)
		{
			/* request input exclusivity from window manager */
			XtVaSetValues(topLevel, XmNmwmInputMode, MWM_INPUT_SYSTEM_MODAL, NULL);
			
			break;
		}
	}


/* center the window if -center arg is passed */
	for (int i = 1; i < argc; i++) 
	{
		if (strcmp(argv[i], "-c") == 0 || strcmp(argv[i], "-center") == 0)
		{
			Display *display = XtDisplay(topLevel);
			Window window = XtWindow(topLevel);
	
			/* check for xinerama extension info */
			int event_base, error_base;
	
			if(!XineramaQueryExtension(display, &event_base, &error_base)) 
			{
				fprintf(stderr, "Notifier: Could not query Xinerama extensions.\n");
				fflush(stderr);
				XtAppMainLoop(app);
				return 1;
			}
	
			/* get screen count */
			int screen_count;
			XineramaScreenInfo *screen_info = XineramaQueryScreens(display, &screen_count);
		
			if(!screen_info || screen_count == 0)
			{
				fprintf(stderr, "Notifier: Could not retrieve Xinerama screen info.\n");
				fflush(stderr);
				XtAppMainLoop(app);
				return 1;
			}
	
			/* get window attributes like height and width */
			XWindowAttributes windowAttr;
			XGetWindowAttributes(display, window, &windowAttr);
			int windowWidth = windowAttr.width;
			int windowHeight = windowAttr.height;
	
			if(screen_count > 0)
			{
				RootWindow(display, screen_info[0].screen_number);
		
				/* take height/width of window, add screen H/W, then divide by two */
				int newX = screen_info[0].x_org + (screen_info[0].width - windowWidth) / 2;
				int newY = screen_info[0].y_org + (screen_info[0].height - windowHeight) / 2;
		
				/* finally move window to center */
				XMoveWindow(display, window, newX, newY);
			}
	
			XFree(screen_info);
			
			break;
		}
	}
	
	/* enter main processing loop */
	XtAppMainLoop(app);
}





